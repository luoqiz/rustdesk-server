# RustDesk服务器程序

[开源实现演示源代码](https://github.com/rustdesk/rustdesk-server-demo)


[下载](https://gitee.com/rustdesk/rustdesk-server/releases)

[文档](https://rustdesk.com/docs/zh-cn/self-host)
